package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    

   private static final double DEFAULT_RATE = 5;  

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   private BankAccount account;
   

   public InvestmentFrame()
   {  
	   rateLabel = new JLabel("Interest Rate: ");
	   button = new JButton("Add Interest");
	   resultLabel = new JLabel("balance: " );
	   
      createTextField();
      createPanel();
      
   }
   
   
   public void setResult(String balance) {
		resultLabel.setText("balance: " + balance);
	}
   public String getrateField(){
		  return rateField.getText();
		   
	   }

   public void createTextField()
   {

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   public void createButton(ActionListener list)
   {
      button.addActionListener(list);
   }

   public void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
}