package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.BankAccount;
import gui.InvestmentFrame;

public class Controller {
	
	/*
		��ҵ�ͧ��С��  Attribute � Controller 
	  	����ջ�С��������������  Bankaccount ��� InvestmentFrame
	  	������� ��ҵ�ͧ��÷��й� Object �ͧ  Bankaccount ��� InvestmentFrame 
	  	�������¡�� ���� Controller ���������ǹ�ͧ Model ��� View �Դ��͡Ѻ Controller
	  	
	*/
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private static final double INITIAL_BALANCE = 1000;
	BankAccount account = new BankAccount(INITIAL_BALANCE);

	 class AddInterestListener implements ActionListener{

		 public void actionPerformed(ActionEvent event)
	       {
			double rate = Double.parseDouble(frame.getrateField());
			double interest = account.getBalance() * rate / 100;
	        account.deposit(interest);
	        frame.setResult(account.getBalance()+"");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

	}

	public static void main(String[] args) {

		new Controller();
	}

	public Controller() {
		frame = new InvestmentFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		list = new AddInterestListener();
		frame.createButton(list);
	}
	

	ActionListener list;
	InvestmentFrame frame;
}
